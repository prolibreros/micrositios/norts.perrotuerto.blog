var timer_refrescar = null,
    woeid = 1,
    tweets_pendientes = [],
    local,
    tweet_last,
    hashtag_actual;

// De https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Synchronous_and_Asynchronous_Requests
function obtener(elemento,tipo) {
  var xhr = new XMLHttpRequest();
  if (tipo == "search") {
    xhr.open("GET", "php/search.php?q=%23" + elemento + "+-filter:retweets&lang=" + lang() + "&result_type=recent&count=100", true);
    hashtag_actual = elemento;
    tweets_pendientes = []
  } else if (tipo == "refresh") {
    xhr.open("GET", "php/search.php?q=%23" + elemento + "+-filter:retweets&lang=" + lang() + "&result_type=recent&count=100&since_id=" + tweet_last, true);
    hashtag_actual = elemento;
  } else if (tipo == "closest")
    xhr.open("GET", "php/trends_closest.php" + elemento, true);
  else
    xhr.open("GET", "php/trends_place.php?id=" + elemento, true);
  xhr.onload = function (e) {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        if (tipo == "search")
          procesamiento(JSON.parse(xhr.responseText));
        else if (tipo == "refresh")
          refrescar(JSON.parse(xhr.responseText));
        else if (tipo == "closest")
          tendencias(JSON.parse(xhr.responseText),"closest");
        else
          tendencias(JSON.parse(xhr.responseText),"place");
      } else {
        console.error(xhr.statusText);
      }
    }
  };
  xhr.onerror = function (e) {
    console.error(xhr.statusText);
  };
  xhr.send(null);
}

// Obtiene el lenguaje
function lang () {
  return (navigator.language || navigator.userLanguage).split("-")[0];
}

// Obtiene la geolocalizacion; de: https://stackoverflow.com/questions/15144111/geolocation-doesnt-work-on-mobiles
function posicion (data) {
  local = {lat:data.latitude,long:data.longitude};
  trends();
}

// Anade los tweets al DOM
function anadir (tweets,resultado) {
  for (i in tweets) {
    var t  = tweets[i];

    // Analiza si está verificado
    function verificar () {
      if(t.user.verified)
        return "<span>&#10003;</span>";
      else
        return "";
    }

    // Busca si tiene lugar
    function lugar () {
      if (t.user.location != "")
        return t.user.location;
      else
        return "s. l.";
    }

    // Obtiene la fecha
    function fecha () {
      var f = new Date(t.created_at);

      // Anade un cero si es necesario
      function cero_extra (i) {
        if (i < 10)
          return "0" + i;
        else
          return i;
      }

      return cero_extra(f.getDate()) + "/" + cero_extra(f.getMonth() + 1) + "/" + f.getFullYear() + " &#183; " + cero_extra(f.getHours()) + ":" + cero_extra(f.getMinutes()) + ":" + cero_extra(f.getSeconds()) + " h";
    }

    // Anade conteos
    function conteos () {
       var conteo = [];

       function adicion (elemento, icono) {
         if (elemento > 0) {
           if (icono == "corazon")
             conteo.push(" &#183; <span>&hearts; " + elemento + "</span>");
           else
             conteo.push(" &#183; <span>&#8634; " + elemento + "</span>");
         }
       }

       adicion(t.favorite_count, "corazon");
       adicion(t.retweet_count, "otro");

       return conteo.join("");
    }

    // Anade hrefs al texto
    function adicion_rutas (texto) {

      // Reemplaza urls, hashtags o menciones
      function reemplazo (conjunto, texto_inicial) {

        for (i in conjunto) {
          if (texto_inicial == "url")
            texto = texto.replace(conjunto[i].url, "<a href='" + conjunto[i].url + "' target='_blank'>" + conjunto[i].url + "</a>");
          else if (texto_inicial == "media") {
            console.log(conjunto[i].type);
//            if (conjunto[i].type == "photo")
              texto = texto.replace(conjunto[i].url, "<a href='" + conjunto[i].url + "' target='_blank'><img class='t_img' src='" + conjunto[i].media_url_https + "' /></a>");
//            else
//              texto = texto.replace(conjunto[i].url, "<a href='" + conjunto[i].url + "' target='_blank'><video class='t_video' controls><source src=''></video></a>");
          }
          else if (texto_inicial == "#")
            texto = texto.replace(texto_inicial + conjunto[i].text, "<a href='https://twitter.com/hashtag/" + conjunto[i].text + "?src=hash' target='_blank'>" + texto_inicial + conjunto[i].text  + "</a>")
          else if (texto_inicial == "@");
            texto = texto.replace(texto_inicial + conjunto[i].screen_name, "<a href='https://twitter.com/" + conjunto[i].screen_name + "' target='_blank'>" + texto_inicial + conjunto[i].screen_name + "</a>");
        }

        return texto;
      }

      texto = reemplazo(t.entities.urls,"url");
      texto = reemplazo(t.entities.hashtags,"#");
      texto = reemplazo(t.entities.user_mentions,"@");
      texto = reemplazo(t.entities.media, "media");

      return texto;
    }

    // Resetea el conjunto
    contenido = [];

    // Estructuracion
    contenido.push("<div id='" + t.id + "'>");
    contenido.push("<p><span>" + t.user.name + "</span><span><a href='https://twitter.com/" + t.user.screen_name + "' target='_blank'>@" + t.user.screen_name + "</a></span>" + verificar() + "</p>");
    contenido.push("<p><span>" + lugar() + "</span> &#183; <span>" + fecha() + "</span>" + conteos() + "</p>")
    contenido.push("<p>" + adicion_rutas(t.text) + "</p>");
    contenido.push("<p><a href='https://twitter.com/" + t.user.screen_name + "/status/" + t.id_str + "' target='_blank'>↗</a></p>");
    contenido.push("</div>");

    // Anade el contenido
    resultado.innerHTML += contenido.join("");
  }
}

// Va obteniendo los nuevos tweets
function refrescar (elemento) {
  var tweets = JSON.parse(elemento).statuses,
      resultado = document.getElementById("resultado"),
      mas_tweets = document.getElementById("mas-tweets");

  // Solo si hay nuevos tweets
  if (tweets.length > 0) {
    // Ordena por fecha, empezando por el mas antiguo; de: https://stackoverflo$
    tweets = tweets.sort(function (a,b) {
      return new Date(a.created_at) - new Date(b.created_at);
    });

    // Elimina tweets repetidos
    if (tweets[0].id == tweet_last)
      tweets.shift();

    // Evita falso positivo por si se elimino un repetido y quedo vacio
    if (tweets.length > 0) {
      // Obtiene el id del tweet mas antiguo
      tweet_last = tweets[tweets.length -1].id;

      // Anade los tweets a la lista de tweets pendientes
      for (i in tweets)
         tweets_pendientes.push(tweets[i]);

      // Elimina el contenido HTML si lo hay
      if (mas_tweets != null)
        mas_tweets.parentNode.removeChild(mas_tweets);

      // Incrusta el contenido HTML
      resultado.innerHTML += "<div id=\"mas-tweets\"><p><a onclick=\"refrescado()\">+" + tweets_pendientes.length + " tweets</a></p></div>";
    }
  }
}

function refrescado () {
  var resultado = document.getElementById("resultado"),
      mas_tweets = document.getElementById("mas-tweets");

  // Detiene el timer
  clearInterval(timer_refrescar);
  
  // Remueve el aviso de nuevos tweets
  mas_tweets.parentNode.removeChild(mas_tweets);

  // Anade los nuevos tweets
  anadir(tweets_pendientes,resultado);

  // Reseta los tweets pendientes
  tweets_pendientes = [];

  // Busca nuevos tweets cada 15 segundos
  timer_refrescar = setInterval(function(){
    obtener(hashtag_actual,"refresh")
  },15000);
}

// Procesa los datos obtenidos
function procesamiento (elemento) {
  var tweets = JSON.parse(elemento).statuses,
      resultado = document.getElementById("resultado");

  console.log(tweets);

  // Detiene el timer
  if (timer_refrescar != null)
    clearInterval(timer_refrescar);

  // Quita la animacion de carga
  resultado.classList.remove("transparente");

  // Analiza si hay elementos
  if (tweets.length == 0)
    resultado.innerHTML = "No hay resultados.";
  else {
    var contenido = [];

    resultado.innerHTML = "";

    // Ordena por fecha, empezando por el mas antiguo; de: https://stackoverflow.com/questions/19430561/how-to-sort-a-javascript-array-of-objects-by-date
    tweets = tweets.sort(function (a,b) {
      return new Date(a.created_at) - new Date(b.created_at);
    });

    // Obtiene el id del tweet mas antiguo
    tweet_last = tweets[tweets.length -1].id;

    // Manda a agregar los tweets    
    anadir(tweets,resultado);
  }

  // Busca nuevos tweets cada 15 segundos
  timer_refrescar = setInterval(function(){
    obtener(hashtag_actual,"refresh")
  },15000);
}

// Verifica que el termino de busqueda no este vacio
function verificar (hashtag) {
  if (hashtag.trim() != "") {

    // Obliga a poner el hashtag en el formulario
    document.getElementById("hashtag").value = hashtag;

    // Acomoda clases al buscador
    var buscador = document.getElementById("buscador");
    buscador.classList.remove("solo");
    buscador.classList.add("acompanado");

    // Elimina el div de los resultados si ya existe
    if (document.getElementById("resultado") != null) {
      var r = document.getElementById("resultado");
      r.parentNode.removeChild(r);
    }

    // Agrega otro div para los resultados
    var resultado = document.createElement("div");
    resultado.id = "resultado";
    resultado.classList.add("transparente");
    resultado.innerHTML = "<p id='buscando'>...</p>";
    document.body.appendChild(resultado);

    // Busca la informacion solicitada
    obtener(hashtag.replace("#",""),"search");
    trends();
  }  
}

// Busca saber quétipo de trend llamar
function trends () {
  if (woeid == 1)
    obtener("?lat=" + local.lat + "&long=" + local.long,"closest");
  else
    obtener(woeid,"place");  
}

// Para procesar las tendencias
function tendencias (elemento,tipo) {

  // Si se identifico la localizacion, se buscan los trends del pais
  if (tipo == "closest") {
    woeid = JSON.parse(elemento)[0].woeid;
    obtener(woeid,"place");
  // Cuando ya se tienen trends, sean locales o globales
  } else {
    var t = typeof JSON.parse(elemento)[0].trends === "undefined" ? null : JSON.parse(elemento)[0].trends,
        trends = [],
        hashtags = document.getElementById("hashtags");

    if (t != null) {
      // Toma los primeros cinco trends con hashtag
      for (i = 0; i < t.length; i++)
        if (/^#/i.test(t[i].name) && trends.length < 5)
          trends.push(t[i].name.substr(1));

      // Agrega los elementos HTML
      hashtags.innerHTML = ""
      for (i in trends)
        hashtags.innerHTML += "<span><a onclick=\"trends();verificar('" + trends[i] + "','search')\">#" + trends[i] + "</a></span> ";

      // Acomoda las alturas
      buscador_altura();
    }
  }
}

// Acomoda las alturas por si los trends ocupan mas lineas
function buscador_altura () {
  var buscador = document.getElementById("buscador"),
      formulario = document.getElementsByTagName("form")[0],
      resultado = document.getElementById("resultado"),
      altura = formulario.offsetHeight + 10;

  if (buscador.classList.contains("acompanado")) {
    buscador.style.height = altura + "px";
    resultado.style.marginTop = "calc( " + altura + "px + 2em)";
  }
}

// Para mostrar los trends
window.onload = function(){
  setInterval(trends,15000);
}

// Acomoda las alturas al cambiarse el tamano de ventana
window.onresize = function() {
  buscador_altura();
}
